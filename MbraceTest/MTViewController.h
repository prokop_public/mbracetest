//
//  MTViewController.h
//  MbraceTest
//
//  Created by Peter Prokop on 18/09/14.
//
//

#import <UIKit/UIKit.h>

@interface MTViewController : UIViewController


@property (weak)        IBOutlet UITableView*       tableView;
@end
