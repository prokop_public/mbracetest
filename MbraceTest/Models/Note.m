//
//  Note.m
//  MbraceTest
//
//  Created by Peter Prokop on 18/09/14.
//
//

#import "Note.h"


@implementation Note

@dynamic object_id;
@dynamic text;
@dynamic order;

@end
