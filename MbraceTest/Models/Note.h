//
//  Note.h
//  MbraceTest
//
//  Created by Peter Prokop on 18/09/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Note : NSManagedObject

@property (nonatomic, retain) NSNumber * object_id;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSNumber * order;

@end
