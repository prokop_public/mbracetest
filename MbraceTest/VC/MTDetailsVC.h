//
//  MTDetailsVC.h
//  MbraceTest
//
//  Created by Peter Prokop on 18/09/14.
//
//

#import <UIKit/UIKit.h>

@interface MTDetailsVC : UIViewController

@property (strong)      NSString*                   text;

@property (strong)      IBOutlet UITextView*        textView;

@end
