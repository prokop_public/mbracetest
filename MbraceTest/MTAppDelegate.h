//
//  MTAppDelegate.h
//  MbraceTest
//
//  Created by Peter Prokop on 18/09/14.
//
//

#import <UIKit/UIKit.h>

@interface MTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
