//
//  Config.h
//  MbraceTest
//
//  Created by Peter Prokop on 18/09/14.
//
//

#ifndef MbraceTest_Config_h
#define MbraceTest_Config_h

/*
 CoreData
 */

#define CONFIG_COREDATA_STORAGE_FILENAME @"MbraceTest.sqlite"
#define CONFIG_COREDATA_MAIN_MODEL_NAME @"Model"

#endif
