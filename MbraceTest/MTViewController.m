//
//  MTViewController.m
//  MbraceTest
//
//  Created by Peter Prokop on 18/09/14.
//
//

#import "MTViewController.h"
#import "MTNotesManager.h"
#import "Note.h"
#import "MTDetailsVC.h"

@interface MTViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    NSArray* _notes;
}
@end

@implementation MTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [[MTNotesManager instance] getNotesWithCallback:^(NSArray *notes) {
        _notes = notes;
        
        [self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue
                 sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"details"]) {
        MTDetailsVC* dvc = segue.destinationViewController;
        dvc.text = ((Note*)sender).text;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Note* note = _notes[indexPath.row];
    
    [self performSegueWithIdentifier:@"details"
                              sender:note];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _notes.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier = @"notes cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:(UITableViewCellStyleDefault)
                                      reuseIdentifier:identifier];
    }
    
    Note* note = _notes[indexPath.row];
    
    NSUInteger length = MIN(40, note.text.length);
    
    cell.textLabel.text = [note.text substringToIndex:length];
    
    return cell;
}

@end
