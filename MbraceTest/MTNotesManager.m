//
//  MTNotesManager.m
//  MbraceTest
//
//  Created by Peter Prokop on 18/09/14.
//
//

#import "MTNotesManager.h"
#import "Config.h"
#import "Note.h"

static dispatch_queue_t contextQueue;

@implementation MTNotesManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

static dispatch_queue_t queue;

+ (MTNotesManager*)instance {
    static dispatch_once_t token;
    static MTNotesManager* instance;
    
    dispatch_once(&token, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (id)init {
    self = [super init];
    
    if(self) {
        static dispatch_once_t token;
        dispatch_once(&token, ^{
            contextQueue = dispatch_queue_create("contextQueue", DISPATCH_QUEUE_SERIAL);
        });

        [self saveNotes];
    }
    
    return self;
}

- (void)saveNotes {
    dispatch_async(contextQueue, ^{
        NSArray *notes = @[@{@"id":@1, @"text": @"First note"},
                           @{@"id":@2, @"text": @"Secon note with a link to http://www.google.de"},
                           @{@"id":@3, @"text": @"Third note"},
                           @{@"id":@4, @"text": @"Fourth note"},
                           @{@"id":@5, @"text": @"Fifth note with an email adress to jakob@mbraceapp.com"},
                           @{@"id":@6, @"text": @"6th note"},
                           @{@"id":@6, @"text": @"6th note updated"},
                           @{@"id":@7, @"text": @"7th note"},
                           @{@"id":@8, @"text": @"8th note"},
                           @{@"id":@9, @"text": @"9th note"},
                           @{@"id":@10, @"text": @"10th note"},
                           @{@"id":@11, @"text": @"11th note"},
                           @{@"id":@12, @"text": @"12th note"},
                           @{@"id":@13, @"text": @"13th note"},
                           @{@"id":@14, @"text": @"14th note"},
                           @{@"id":@15, @"text": @"get mbrace at http://www.getmbrace.com"},
                           @{@"id":@16, @"text": @"16th note"},
                           @{@"id":@17, @"text": @"17th note"},
                           @{@"id":@18, @"text": @"18th note"},
                           @{@"id":@19, @"text": @"19th note"},
                           @{@"id":@20, @"text": @"20th note"},
                           @{@"id":@21, @"text": [NSNull null]},
                           @{@"id":@22, @"text": @"22th note"},
                           @{@"id":@23, @"text": @"23th note"},
                           @{@"id":@24, @"text": @"Visit www.mbraceapp.com"},
                           @{@"id":@25, @"text": @"25th note"},
                           @{@"id":@26, @"text": @"Note that is a little bit longer than all the other notes because of consiting of some strings that are useless and take a lot of space"},
                           @{@"id":@27, @"text": @"27th note"},
                           @{@"id":@28, @"text": @"28th note"},
                           @{@"id":@29, @"text": @"29th note"},
                           @{@"id":@30, @"text": @"another email to lukas@mbraceapp.com"},
                           @{@"id":@31, @"text": @"31th note"},
                           @{@"id":@32, @"text": @"32th note"},
                           @{@"id":@33, @"text": @"33th note"},
                           @{@"id":@34, @"text": @"almost at the end note"},
                           @{@"id":@35, @"text": @"Last note note"},
                           @{@"id":@12, @"text": @"Updated 12th note"}];
        
        NSManagedObjectContext* context = [self managedObjectContext];
        
        
        NSInteger i = 0;
        for(NSDictionary* dic in notes) {
            if(![dic isKindOfClass:[NSDictionary class]])
                continue;
            
            Note* note;
            
            NSNumber* objectID = [dic objectForKey:@"id"];
            if([objectID isKindOfClass:[NSNumber class]]) {
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"Note"
                                                          inManagedObjectContext:context];
                [fetchRequest setEntity:entity];
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(object_id = %@)", objectID];
                [fetchRequest setPredicate:predicate];
                
                NSError *error;
                NSArray *array = [context executeFetchRequest:fetchRequest error:&error];
                
                if (array && [array count]) {
                    note = array[0];
                } else {
                    note = [NSEntityDescription insertNewObjectForEntityForName:@"Note"
                                                         inManagedObjectContext:context];
                    
                    note.object_id = objectID;
                    note.order = @(i);
                    i++;
                }
            } else
                continue;
            
            NSString* text = [dic objectForKey:@"text"];
            if([text isKindOfClass:[NSString class]]) {
                note.text = text;
            }
        }
        
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Error, couldn't save to CoreData: %@", [error localizedDescription]);
        }
    });
}

- (void)getNotesWithCallback:(void (^)(NSArray* notes))callback {
    dispatch_async(contextQueue, ^{
        NSManagedObjectContext* context = [self managedObjectContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Note"
                                                  inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        
        // Sort
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order" ascending:YES];
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
        
        NSError* error;
        NSArray *arr = [context executeFetchRequest:fetchRequest error:&error];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            callback(arr);
        });
    });
}

#pragma mark - Core Data

- (void)saveContext {
    NSError* error = nil;
    NSManagedObjectContext* managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"An error has occured: %@, %@", error, [error userInfo]);
        }
    }
}

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL* modelURL = [[NSBundle mainBundle] URLForResource:CONFIG_COREDATA_MAIN_MODEL_NAME
                                              withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL* applicationDocumentsDirectory = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                                                   inDomains:NSUserDomainMask] lastObject];
    
    NSURL* storeURL = [applicationDocumentsDirectory URLByAppendingPathComponent:CONFIG_COREDATA_STORAGE_FILENAME];
    
    NSError* error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeURL
                                                         options:nil
                                                           error:&error])
    {
        NSLog(@"An error has occured: %@, %@", error, [error userInfo]);
    }
    
    return _persistentStoreCoordinator;
}

@end
