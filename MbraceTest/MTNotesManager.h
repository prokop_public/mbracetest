//
//  MTNotesManager.h
//  MbraceTest
//
//  Created by Peter Prokop on 18/09/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface MTNotesManager : NSObject

+ (MTNotesManager*)instance;

// Core Data
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)getNotesWithCallback:(void (^)(NSArray* notes))callback;

@end
